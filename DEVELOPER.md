# Reminders for the developer
You'll need docker and an ssh server on your development host.
Start a shell in a tiakigazetteer docker machine, eg: 
```
docker login registry.gitlab.com
docker pull registry.gitlab.com/axrdo/tiakigazetteer
docker run -it registry.gitlab.com/axrdo/tiakigazetteer /bin/bash

```
Clone the tiakigazetteer project into the machine, and install it
```
git clone https://gitlab.com/axrdo/tiakigazetteer.git
cd tiakigazetteer
npm run i
```
The editor `nano` is installed for making changes.
To update the tiaki data from your development host, scp it eg:
```
scp axrdo@172.17.0.1:Downloads/EMuGeographicNamemetadataIRNandNZGBPlaceID.xlsx .
```

To try out your changes
```
rm places.json tiaki.json
make publish
npm run build
npm run start
```
This will regenerate everthing and bring up the gazetteer locally for you to surf to.
You'll have to set your git user eg
```
git config user.email your@email.address
git config user.name "Your name"
```
to be able to git add, commit and push. Good Luck!
