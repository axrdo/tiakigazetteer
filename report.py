#!/usr/bin/env python3

import argparse
import csv
import json
import logging
import sys

parser = argparse.ArgumentParser()
parser.add_argument(
    "tiakigazetteer", help="the input: the Tiaki NZ gazetteer geojson")
parser.add_argument("report", help="the output csv report")
parser.add_argument(
    "differences", help="just the rows with differences between the NZGB name and the Tiaki name")
args = parser.parse_args()

j = open(args.tiakigazetteer, "r")
gazetteer = json.load(j)
result = []
for f in gazetteer["features"]:
    feat_id = f["id"]
    nn = f["properties"]["name_id"]
    for n in nn:
        name_id = nn[n]["name_id"]
        name = nn[n]["name"]
        ii = nn[n]["irn"]
        for i in ii:
            irn = ii[i]["irn"]
            place = ii[i]["place"]
            result.append(
                [feat_id,
                    None,
                    name_id,
                    None,
                    irn,
                    name,
                    place
                 ]
            )
j.close

result.sort()
fieldnames = ["feat_id", "n", "name_id", "i", "irn", "name", "place"]

reportfile = open(args.report, 'w', newline='')
reportwriter = csv.writer(reportfile)
reportwriter.writerow(fieldnames)
r = 0
result[r][1] = 1
result[r][3] = 1
reportwriter.writerow(result[r])
for r in range(1, len(result)-1):
    if result[r][0] == result[r-1][0]:
        result[r][1] = result[r-1][1] + 1
    else:
        result[r][1] = 1
    if result[r][2] == result[r-1][2]:
        result[r][3] = result[r-1][3] + 1
    else:
        result[r][3] = 1
    reportwriter.writerow(result[r])
reportfile.close()

differencesfile = open(args.differences, 'w', newline='')
differenceswriter = csv.writer(differencesfile)
differenceswriter.writerow(fieldnames)
for r in range(0, len(result)):
    if result[r][5] != result[r][6]:
        differenceswriter.writerow(result[r])
differencesfile.close()
