.PHONY: publish
publish: public/bundle.js public/tiakigazetteer.csv

places.json:
	./tiaki.py 'EMuGeographicNamemetadataIRNandNZGBPlaceID.xlsx' >$@
features.json names.json: 
	./gazetteer.py --features=features.json --names=names.json nz-place-names-nzgb/nz-place-names-nzgb.csv
public/tiakigazetteer.geojson public/warnings.csv: features.json names.json places.json
	./join.py features.json names.json places.json public/warnings.csv >public/tiakigazetteer.geojson
public/tiakigazetteer.csv public/differences.csv: public/tiakigazetteer.geojson
	./report.py $< public/tiakigazetteer.csv public/differences.csv
public/bundle.css public/bundle.js:
	npm install
	npm run build
