#!/usr/bin/env python3

import argparse
import csv
import geodaisy.converters as convert
import logging
import json
import re
import sys

parser = argparse.ArgumentParser()
parser.add_argument("placenames", help="NZ Place Names (NZGB) in CSV with WGS 84 projection")
parser.add_argument("--features", help="(output) a json dictionary of features keyed by feat_id")
parser.add_argument("--names", help="(output) a json dictionary of names keyed by name_id")
args = parser.parse_args()

regex = re.compile(r"POINT \(([\-0123456789.]+) ([\-0123456789.]+)\)$")
if args.features:
    result={}
    with open(args.placenames, "r", encoding="utf-8", newline='') as placenamesfile:
        placenamesreader = csv.reader(placenamesfile)
        for row in placenamesreader:
          if (row[0]!="WKT" and row[4]!="feat_id"):
            if row[4] in result:
                assert(row[0] == result[row[4]]["properties"]["WKT"])
            else:
                match = regex.match(row[0])
                assert(match is not None)
                result[row[4]]={
                          "type": "Feature",
                          "id": int(row[4]),
                          "geometry":{
                              "type": "Point",
                              "coordinates": [float(match.group(1)), float(match.group(2))],
                           },
                          "properties":{
                              "WKT": row[0]
                           }
                    }
    features = open(args.features, "w")
    json.dump(result, features, ensure_ascii=False, indent=1)
    placenamesfile.close()

if args.names:
    result={}
    with open(args.placenames, "r", encoding="utf-8", newline='') as placenamesfile:
        placenamesreader = csv.reader(placenamesfile)
        for row in placenamesreader:
            if (row[0]!="WKT" and row[2]!="name_id" and row[4]!="feat_id"):
                assert(row[1] not in result)
                result[row[1]]={
                          "name_id": int(row[1]),
                          "name": row[2],
                          "feat_id": int(row[4])
                    }
    names = open(args.names, "w")
    json.dump(result, names, ensure_ascii=False, indent=1)
    placenamesfile.close()
