Task by task documentation for project maintainers.

# Who are Project Maintainers?
Project maintainers are project members https://gitlab.com/axrdo/tiakigazetteer/-/project_members with Max role of Maintainer.
Please see GUEST.md if you are not a maintainer but wish to perform any of these tasks.

How do I update the gazetteer with new Tiaki placename information?
There are 3 steps:

## obtain Tiaki information in the form required
You will need to have access to the Tiaki application at ATL, and then:

>>>
I have just created a new List View setting in EMu to ensure that retrieval of an updated report will be very easy going forward.
All one has to do now is populate the Authority field in the Geographic Authority group in Reference tab of Thesaurus module, and select the New Zealand Geographic Board from drop-down menu. Searching on this single field then returns the required metadata, and may be copied into the spreadsheet after using the new List Setting Geographic Name IRN and NZGB ID. (@SaschaNolden)
>>>
You will need to ensure that the resulting spreadsheet is called `EMuGeographicNamemetadataIRNandNZGBPlaceID.xlsx`

## Replace the spreadsheet from EMu in this project
Find the EMu spreadsheet "EMuGeographicNamemetadataIRNandNZGBPlaceID.xlsx" on the project home page at https://gitlab.com/axrdo/tiakigazetteer and click on it 
(or go directly to https://gitlab.com/axrdo/tiakigazetteer/-/blob/main/EMuGeographicNamemetadataIRNandNZGBPlaceID.xlsx)
Click the `Replace` button, bringing up the "Replace EMuGeographicNamemetadataIRNandNZGBPlaceID.xlsx" dialog. You can modify the Commit message as you like, but please leave the Target branch saying "main" alone (the main branch is the one from which the map is built).

## The CI/CD pipeline runs automatically... we hope!
From the project home page go to **CI/CD > Pipelines**. In a few minutes you should be seeing "passed" in green, your commit message and green ticks for the two stages of the pipeline. More about this in the Gitlab software documentation here: https://docs.gitlab.com/ee/ci/pipelines/#view-pipelines  

## Confirm 
Now https://axrdo.gitlab.io/tiakigazetteer/ should reflect the new data: it will help if you know a placename you can search for that will confirm the update. https://axrdo.gitlab.io/tiakigazetteer/warnings.csv will also have been renewed: any rows in this report should be investigated. See GUEST.md for more ... 

